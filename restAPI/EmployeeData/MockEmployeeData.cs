﻿using System;
using System.Collections.Generic;
using restAPI.Models;

namespace restAPI.EmployeeData
{
    public class MockEmployeeData : IEmployeeDataInterface
    {
        private List<Employee> employees = new List<Employee>()
        {
            new Employee()
            {
                    Id = Guid.NewGuid(),
                    Name = "Employee One"
            },

              new Employee()
            {
                    Id = Guid.NewGuid(),
                    Name = "Employee Two"
            },
        };



        public Employee AddEmployee(Employee employee)
        {
            throw new NotImplementedException();
        }

        public void DeleteEmployee(Employee employee)
        {
            throw new NotImplementedException();
        }

        public Employee EditEmployee(Employee employee)
        {
            throw new NotImplementedException();
        }

        public Employee GetEmployee(Guid id)
        {
            throw new NotImplementedException();
        }

        public List<Employee> GetEmployees()
        {
            return employees;
        }
    }
}
