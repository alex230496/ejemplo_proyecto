﻿using System;
using System.Collections.Generic;
using restAPI.Models;

namespace restAPI.EmployeeData
{
    public interface IEmployeeDataInterface
    {
        List<Employee> GetEmployees();
        //Methods para trabajar con los empleados
        Employee GetEmployee(Guid id);

        Employee AddEmployee(Employee employee);

        void DeleteEmployee(Employee employee);

        Employee EditEmployee(Employee employee);

        
    }
}
