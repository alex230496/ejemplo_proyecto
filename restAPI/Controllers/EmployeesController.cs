﻿using Microsoft.AspNetCore.Mvc;
using restAPI.EmployeeData;


namespace restAPI.Controllers
{
    [Route("api/[controller]")]
    public class EmployeesController : Controller
    {
        private IEmployeeDataInterface _employeeData;
        public EmployeesController(IEmployeeDataInterface employeeData)
        {
            _employeeData = employeeData;

        }

        [HttpGet]
        [Route("api/[controller]")]
        public IActionResult GetEmployees()
        {
            return Ok(_employeeData.GetEmployees());
        }
    }
}
